class ChangeDefaultForDisplayInTooltip < ActiveRecord::Migration[7.1]
  def change
    change_column_default :fields, :display_in_tooltip, from: false, to: true
  end
end
