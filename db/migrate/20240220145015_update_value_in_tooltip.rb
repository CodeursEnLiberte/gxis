class UpdateValueInTooltip < ActiveRecord::Migration[7.1]
  disable_ddl_transaction!

  def up
    Layer.find_each do |layer|
      if layer.fields.all? { |field| field.display_in_tooltip == false }
        layer.fields.each do |field|
          field.update(display_in_tooltip: true)
        end
      end
    end
  end
end
