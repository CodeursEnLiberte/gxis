# Anonymous users are instances of MapTokenAuthenticatable::AnonymousUser, and can never be stored in database.
class MapTokenAuthenticatable::AnonymousUser < ::User
  attr_accessor :anonymous_tag # identifies the user across queries (saved in the session cookie)
  attr_accessor :tokens_array  # stores the known map tokens (saved in the session cookie, too.)

  def initialize(anonymous_tag = nil)
    @anonymous_tag = anonymous_tag || SecureRandom.alphanumeric
    super(remember_me: false)
  end

  def anonymous? = true

  COCARTO_ANONYMOUS_MAP_TOKENS = "cocarto.anonymous.map_tokens"

  def store_tokens_array_in_session(session)
    session[COCARTO_ANONYMOUS_MAP_TOKENS] ||= []
    # NOTE: the tokens_array is shared state between the AnonymousUser and the Session serializer,
    # it is *not* copied here, and it is *mutable*.
    # Token strings added to the tokens_array will be stored in the session cookie.
    self.tokens_array = session[COCARTO_ANONYMOUS_MAP_TOKENS]
  end

  def map_tokens
    MapToken.where(token: tokens_array)
  end

  def map_tokens=(tokens)
    self.tokens_array = tokens.map(&:token)
  end

  def to_global_id(options = {}) # Needed to identify the connection in ApplicationCable
    anonymous_tag
  end

  ## Methods overridden from User
  #
  def display_name
    I18n.t("users.anonymous")
  end

  def access_for_map(map)
    map_tokens.find { _1.map_id == map.id }
  end

  def assign_map_token(map_token)
    existing_map_token = map_tokens.find { _1.map_id == map_token.map_id }
    return if existing_map_token == map_token

    map_token.increment!(:access_count) # rubocop:disable Rails/SkipsModelValidations

    if existing_map_token.nil? # self does not already have access to the map
      tokens_array << map_token.token
    elsif map_token.is_stronger_than(existing_map_token) # self already has a lower access to the map
      tokens_array.delete(existing_map_token.token)
      tokens_array << map_token.token
    end
  end

  def maps
    Map.where(map_tokens: map_tokens)
  end

  def maps_with_role_at_least(role_type)
    Map.where(map_tokens: map_tokens.where(role_type: RoleType.roles_at_least(role_type)))
  end
end
