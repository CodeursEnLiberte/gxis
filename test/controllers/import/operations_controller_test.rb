require "test_helper"

class Import::OperationsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test "#index access" do
    sign_in users(:reclus)
    get map_import_operations_path(maps(:restaurants))

    assert_response :success
  end
end
