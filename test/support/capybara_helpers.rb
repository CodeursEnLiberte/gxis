module Capybara::Helpers::Cocarto
  def sign_in_as(user, password)
    visit user_session_path
    fill_in "user_email", with: user.email
    fill_in "user_password", with: password
    click_button "Log in"

    assert_text "Signed in successfully", wait: 10
  end

  def sign_out
    wait_until_dropdown_controller_ready
    find('[title="Settings"]').click
    click_button "Sign out"

    assert_text "Signed out successfully"
    visit("/")
  end

  def clear_session
    page.driver.browser.cookies.clear
    wait_until { page.driver.browser.cookies["_cocarto_session"].nil? }
  end

  # We want to make sure that the dropdown controller is loaded
  # Otherwise capybara will click on the button, and nothing happens
  def wait_until_dropdown_controller_ready
    within(".header__container") { find_dropdown }
  end

  def find_dropdown(**options)
    find("[data-dropdown-controller=connected]", **options, wait: 10)
  end

  def wait_until_map_loaded
    find ".maplibregl-map[data-loaded]", wait: 10
  end

  def wait_until_turbo_stream_connected
    # Turbo adds the `connected` attribute to <turbo-cable-stream-source> elements when the stream is up
    # We can rely on this to ensure row broadcasts from the backend are actually received.
    # (match: :first is needed because of turbo_stream_i18n_from. We can remove it after #196 if we stream empty frame-tags.)
    find "turbo-cable-stream-source[channel='Turbo::StreamsChannel'][connected]", match: :first, wait: 10
  end

  def rm_downloaded_file(name)
    downloads = Pathname.new(Capybara.save_path)
    File.delete(downloads.join(name))
  rescue
    nil
  end

  def wait_until_downloaded_file(name)
    downloads = Pathname.new(Capybara.save_path)
    wait_until { downloads.glob("*.crdownload").blank? && File.exist?(downloads.join(name)) }

    assert_path_exists downloads.join(name)
  end

  def wait_until(time = Capybara.default_max_wait_time)
    Timeout.timeout(time) { sleep(0.05) until yield }
  end
end
