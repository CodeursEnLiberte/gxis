# Preview all emails at https://localhost:3000/rails/mailers/user_roles_mailer
class UserRolesMailerPreview < ActionMailer::Preview
  def invited
    user = User.first
    user_role = UserRole.includes(:map, :user).first
    UserRolesMailer.with(user_role: user_role, inviter: user).invite
  end
end
