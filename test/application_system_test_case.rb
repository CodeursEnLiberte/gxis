require "test_helper"
require "capybara/cuprite"
require "support/capybara_helpers"
require "support/ferrum_logger"
require "minitest/retry"

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  include Capybara::Email::DSL
  include Capybara::Helpers::Cocarto
  Minitest::Retry.use!

  # Allow connections from the local network
  Capybara.server_host = "0.0.0.0"
  # Run server in https (using a self-signed certificate, automatically with the localhost gem)
  Capybara.server = :puma, {Host: "ssl://#{Capybara.server_host}", Silent: true}
  # Specify the server hostname (the local machine) where the (remote) browser will connect.
  Capybara.app_host = "https://#{`hostname`.strip&.downcase}"
  # Where will capybara save screenshots, downloaded files…
  Capybara.save_path = ENV.fetch("CAPYBARA_ARTIFACTS", "./tmp/capybara")
  # Disable CSS animations and transitions
  Capybara.disable_animation

  driver_options = {
    logger: FerrumLogger.new,
    js_errors: true,
    headless: ENV["COCARTO_DEBUG_SYSTEM_TESTS_BROWSER"].blank?,
    browser_options: {
      "accept-lang": "en"
    }
  }
  # In the CI, the browser is a chrome service (not a process launched by cuprite)
  if ENV["CI"] == "true"
    driver_options[:url] = "http://browserless-chrome:3000"
    driver_options[:browser_options][:"no-sandbox"] = nil # required for running in Docker
  end

  driven_by :cuprite, screen_size: [1200, 800], options: driver_options

  setup do
    # Use the Capybara host for generating absolute URLs (e.g. when sending emails).
    @original_url_options = Rails.application.default_url_options.slice(:protocol, :host, :port)
    Rails.application.default_url_options = {
      protocol: "https",
      host: URI.parse(Capybara.app_host).host,
      port: Capybara.current_session.server.port
    }

    # Our self-signed certificate is invalid; make Chrome ignore this.
    # https://chromedevtools.github.io/devtools-protocol/tot/Security/#method-setIgnoreCertificateErrors
    # Note: this can be done with browser_options when running locally, because Cuprite/Ferrum launches a new Chrome process.
    # However in CI, we connect to an already-running browser, which we have to configure via the Chrome Devtools Protocol.
    Capybara.current_session.driver.browser.command("Security.setIgnoreCertificateErrors", ignore: true)
  end

  teardown do
    Rails.application.default_url_options.merge(@original_url_options)
  end
end
