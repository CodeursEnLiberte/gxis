ENV["RAILS_ENV"] ||= "test"
require_relative "../config/environment"
require "rails/test_help"

require "minitest/reporters"
Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new if ENV["RM_INFO"].blank? # IntelliJ Minitest support conflicts with Minitest::Reporters

require "fixtures/fixtures_server"
require "support/fix_objc_runtime_initialization"

class ActiveSupport::TestCase
  include FixturesServer

  # Run tests in parallel with specified workers
  parallelize(workers: :number_of_processors)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Use file fixtures as ActiveStorage attachments
  def attachable_fixture(filename)
    {io: file_fixture(filename).open, filename: filename, content_type: Mime[File.extname(filename)[1..]]}
  end

  parallelize_teardown do |_i|
    FileUtils.rm_rf(ActiveStorage::Blob.services.fetch(:test_fixtures).root)
  end

  # Use inline strings as ActiveStorage attachments
  def attachable_data(filename, data)
    {io: StringIO.new(data), filename: filename, content_type: Mime[File.extname(filename)[1..]]}
  end

  # Setup the whole layer/mapping/map/config for an Importer
  def preconfigured_import(layer_fixture_name, source_type, source)
    layer = layers(layer_fixture_name)
    mapping = layer.import_mappings.new
    config = layer.map.import_configurations.new(source_type: source_type, mappings: [mapping])
    importer = config.importer(source, nil, nil)
    config.configure_from_analysis(config.analysis(importer))
    mapping.configure_from_analysis(mapping.analysis(importer))

    [config, mapping]
  end

  def use_memory_store!
    @old_cache = Rails.cache
    memory_cache = ActiveSupport::Cache.lookup_store(:memory_store)

    Rails.cache = memory_cache
    ActionController::Base.cache_store = memory_cache
    ActionView::CollectionRenderer.collection_cache = memory_cache
    ActionController::Base.perform_caching = true

    # Use `singleton_class` (rather than `class`) to avoid
    # adding another teardown hook to all instances of the test class.
    singleton_class.teardown do
      Rails.cache = @old_cache
      ActionController::Base.cache_store = @old_cache
      ActionView::CollectionRenderer.collection_cache = @old_cache
      ActionController::Base.perform_caching = Rails.application.config.action_controller.perform_caching
    end
  end
end

class ActionDispatch::IntegrationTest
  # Make the Controllers tests run in english by default.
  # This also makes the url helper methods (e.g. `layer_url`) easier to use,
  # because the first parameter can be implicitly the model.
  def setup
    self.default_url_options = {locale: I18n.default_locale}
  end
end
