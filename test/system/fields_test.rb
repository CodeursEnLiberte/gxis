require "application_system_test_case"

class FieldsTest < ApplicationSystemTestCase
  # Note / fun fact for the field names in the table headers:
  # Capybara sees the text as modified by `text-transform: uppercase`.
  # Even if the html is actually “Some Text”, we need to look for “SOME TEXT”.
  test "create, edit and destroy a field" do # rubocop:disable Minitest/MultipleAssertions
    sign_in_as(users("reclus"), "refleurir")
    visit map_path(id: maps("restaurants"))
    wait_until_turbo_stream_connected
    wait_until_map_loaded

    # Create
    click_on "New column"
    fill_in "field[label]", with: "Un beau nombre"
    choose "Decimal"

    click_on "OK"

    assert_selector ".layer-table__th--field", text: "UN BEAU NOMBRE"

    # Edit
    find_dropdown(text: "UN BEAU NOMBRE").click

    fill_in "field[label]", with: "Un nombre renommé"
    click_on "OK"

    refute_selector ".layer-table__th--field", text: "UN BEAU NOMBRE"
    assert_selector ".layer-table__th--field", text: "UN NOMBRE RENOMMÉ"

    # Destroy
    find_dropdown(text: "TABLE SIZE").click

    within(".dropdown__content") do
      click_on "Delete"
    end

    refute_selector ".layer-table__th--field", text: "TABLE SIZE"
  end

  test "Make a text field long" do # rubocop:disable Minitest/MultipleAssertions
    sign_in_as(users("reclus"), "refleurir")
    visit map_path(id: maps("restaurants"))
    wait_until_turbo_stream_connected
    wait_until_map_loaded

    assert_selector("input[value='L’Antipode']")

    # Make “Name” long
    find_dropdown(text: "NAME").click
    check "field[text_is_long]"
    click_on "OK"

    assert_link "L’Antipode"
    refute_selector "input[value='L’Antipode']"

    click_on "L’Antipode"

    assert_field "Name", type: :textarea
    fill_in "Name", type: :textarea, with: "Nouveau nom!"
    click_on "Save"

    assert_link "Nouveau nom!"
  end

  test "Lock and unlock a field" do # rubocop:disable Minitest/MultipleAssertions
    sign_in_as(users("reclus"), "refleurir")
    visit map_path(id: maps("restaurants"))
    wait_until_turbo_stream_connected
    wait_until_map_loaded

    within("##{dom_id(rows(:antipode))}") do
      assert_field "row[fields_values][#{fields("restaurant_name").id}]", disabled: false
    end

    # Lock the “Name” field
    find_dropdown(text: "NAME").click
    check "field[locked]"
    click_on "OK"

    within("##{dom_id(rows(:antipode))}") do
      assert_field "row[fields_values][#{fields("restaurant_name").id}]", disabled: true
    end

    # UnLock the “Name” field
    find_dropdown(text: "NAME").click
    uncheck "field[locked]"
    click_on "OK"

    within("##{dom_id(rows(:antipode))}") do
      assert_field "row[fields_values][#{fields("restaurant_name").id}]", disabled: false
    end
  end

  test "Sort by text field" do
    sign_in_as(users("reclus"), "refleurir")
    visit map_path(id: maps("ordering"))
    wait_until_turbo_stream_connected
    wait_until_map_loaded

    # Order by text
    text_field = "row[fields_values][#{fields("text_ordering").id}]"
    find_dropdown(text: "TEXT").click
    click_on "ascending"
    # We make sure that the table finished loading
    find_field(text_field, with: "AAA")

    assert_operator page.body.index("AAA"), :<, page.body.index("zzz")

    find_dropdown(text: "TEXT").click
    click_on "descending"
    find_field(text_field, with: "AAA")

    assert_operator page.body.index("zzz"), :<, page.body.index("AAA")
  end

  test "Sort by number field" do
    sign_in_as(users("reclus"), "refleurir")
    visit map_path(id: maps("ordering"))
    wait_until_turbo_stream_connected
    wait_until_map_loaded

    # We make sure the order is numerical, not lexical
    number_field = "row[fields_values][#{fields("number_ordering").id}]"
    find_dropdown(text: "NUMBER").click
    click_on "ascending"
    # We make sure that the table finished loading
    find_field(number_field, with: "2468")

    assert_operator page.body.index("2468"), :<, page.body.index("13579")

    find_dropdown(text: "NUMBER").click
    click_on "descending"
    find_field(number_field, with: "2468")

    assert_operator page.body.index("13579"), :<, page.body.index("2468")
  end
end
