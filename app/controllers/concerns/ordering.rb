module Ordering
  extend ActiveSupport::Concern

  attr_reader :row_order

  included do
    before_action :set_row_order_from_params
  end

  def set_row_order_from_params
    # This module is included in a controller, so we have access to the params method
    @row_order ||= Order.from_params(params)
  end

  class Order
    attr_accessor :field, :direction
    def initialize
      @field = nil
      @direction = nil
    end

    def self.from_params(params)
      order = new
      order_by_param = params.delete(:order_by)
      if order_by_param
        # order_by is a query param such as order_by[field_id]=ASC
        # It is parsed as hash {field_id => "ASC"}
        # We only handle (for now) one sorting direction, hence the .first
        # We don’t call .permit, so we need to do an unsafe conversion to hash
        field_id, direction = order_by_param.to_unsafe_h.first
        field = Field.find(field_id)
        # We check that the parameters are valid
        if ["ASC", "DESC"].include?(direction) && field.present?
          order.field = field
          order.direction = direction
        end
      end
      order
    end

    def asc? = @direction == "ASC"

    def desc? = @direction == "DESC"

    def sql
      if @field && @direction
        cast = @field.numeric? ? "::numeric" : ""
        Arel.sql("(rows.values->>'#{@field.id}')#{cast} #{direction}")
      else
        :created_at
      end
    end

    def direction_for_field(field)
      if field == @field
        @direction
      else
        ""
      end
    end
  end
end
