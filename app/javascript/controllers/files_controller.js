import { Controller } from '@hotwired/stimulus'
import * as ActiveStorage from '@rails/activestorage'
import bytesToHuman from 'lib/bytes_to_human'

// Multipurpose controller for all things related to file inputs.
// - Using a custom style for the input and display the list of files being uploaded
// - handle file removal
// - drag and drop
// - direct upload
// The calling DOM includes one or several `.file-item` elements.
// This controller adds the following classes to the relevant `.file-items`:
// - files-item--present
// - files-item--removed
// - files-item--added
// - files-item--upload-pending
// - files-item--uploading
// - files-item--upload-error
// - files-item--upload-complete
export default class extends Controller {
  static targets = ['list', 'dropArea', 'fileInput', 'cameraInput', 'template']
  static values = {
    autosubmit: Boolean,
    types: Array,
    maxSize: Number,
    itemRemovedText: String,
    itemAddedText: String,
    itemCompleteText: String,
    itemTooBigText: String
  }

  connect () {
    this.dropAreaTarget.addEventListener('dragover', e => this.dragStarted(e))
    this.dropAreaTarget.addEventListener('dragenter', e => this.dragStarted(e))
    this.dropAreaTarget.addEventListener('dragleave', e => this.dragLeft(e))
    this.dropAreaTarget.addEventListener('drop', e => this.dropped(e))

    if (this.hasCameraInputTarget) {
      if (this.cameraInputTarget.capture === undefined) {
        this.cameraInputTarget.closest('.files-item--button').hidden = true
      }
    }

    const inputs = this.hasCameraInputTarget ? [this.fileInputTarget, this.cameraInputTarget] : [this.fileInputTarget]
    for (const input of inputs) {
      input.addEventListener('change', e => this.updateAddedItems(e))
      input.addEventListener('direct-upload:initialize', e => this.directUploadInitialized(e))
      input.addEventListener('direct-upload:start', e => this.directUploadStarted(e))
      input.addEventListener('direct-upload:progress', e => this.directUploadProgressed(e))
      input.addEventListener('direct-upload:error', e => this.directUploadFailed(e))
      input.addEventListener('direct-upload:end', e => this.directUploadCompleted(e))
    }

    ActiveStorage.start() // ActiveStorage listens to forms submissions (with the data-direct-uploads-processing attribute). Calling start() several times is ok; there is no stop().
  }

  // Actions
  removeItem (event) {
    const item = event.target.closest('.files-item')
    item.classList.remove('files-item--present')
    item.classList.add('files-item--removed')
    item.querySelector('input[type=hidden]').remove()
    item.querySelector('.files-item__remove-button').remove()
    item.querySelector('.files-item__status').innerText = this.itemRemovedTextValue
  }

  // Drag and drop listeners
  dragStarted (event) {
    if (event.dataTransfer.types.includes('Files')) { // Are we dragging files? otherwise (e.g. html or text content from someplace else in the page), ignore.
      event.preventDefault()
      this.dropAreaTarget.classList.add('files-drop-area--active')
    }
  }

  dragLeft (_) {
    this.dropAreaTarget.classList.remove('files-drop-area--active')
  }

  dropped (event) {
    event.preventDefault()
    this.dropAreaTarget.classList.remove('files-drop-area--active')

    for (const file of event.dataTransfer.files) {
      if (this.hasTypesValue && !this.typesValue.includes(file.type)) {
        return
      }
    }

    this.fileInputTarget.files = event.dataTransfer.files
    this.updateAddedItems(event)
  }

  // Update the list of files-item--added
  updateAddedItems (_) {
    this.listTarget.querySelectorAll('.files-item--added').forEach(el => el.remove())

    const files = this.hasCameraInputTarget
      ? Array.from(this.fileInputTarget.files).concat(Array.from(this.cameraInputTarget.files))
      : this.fileInputTarget.files
    const firstButton = this.listTarget.querySelector('.files-item--button')
    for (const file of files) {
      if (file.size > this.maxSizeValue) {
        // Complain and reject everything if one file is too big; let’s not have partially successful situations.
        window.alert(this.itemTooBigTextValue)
        this.fileInputTarget.value = ''
        if (this.hasCameraInputTarget) {
          this.cameraInputTarget.value = ''
        }
        return
      }
      const clone = document.importNode(this.templateTarget.content, true)
      clone.querySelector('.files-item__title').innerText = file.name
      clone.querySelector('.files-item__metadata').innerText = bytesToHuman(file.size)
      clone.querySelector('.files-item').classList.add('files-item--added')
      clone.querySelector('.files-item__status').innerText = this.itemAddedTextValue
      this.listTarget.insertBefore(clone, firstButton)
    }

    if (this.autosubmitValue) {
      this.fileInputTarget.closest('.files-item').hidden = true
      if (this.hasCameraInputTarget) {
        this.cameraInputTarget.closest('.files-item').hidden = true
      }
    }

    if (this.autosubmitValue) {
      this.fileInputTarget.form.requestSubmit()
    }
  }

  // Direct upload listeners
  directUploadInitialized (event) {
    const { detail } = event
    const { id, file } = detail

    this.listTarget.querySelectorAll('.files-item--added').forEach(el => el.remove())
    const firstButton = this.listTarget.querySelector('.files-item--button')

    const clone = document.importNode(this.templateTarget.content, true)
    clone.querySelector('.files-item__title').innerText = file.name
    clone.querySelector('.files-item__metadata').innerText = bytesToHuman(file.size)
    clone.querySelector('.files-item').classList.add('files-item--upload-pending')
    clone.querySelector('.files-item').id = `files-item-direct-upload-${id}`
    this.listTarget.insertBefore(clone, firstButton)
  }

  directUploadStarted (event) {
    const { id } = event.detail
    const item = this.listTarget.querySelector(`#files-item-direct-upload-${id}`)
    item.classList.remove('files-item--upload-pending')
    item.classList.add('files-item--uploading')
  }

  directUploadProgressed (event) {
    const { id, progress } = event.detail
    const item = this.listTarget.querySelector(`#files-item-direct-upload-${id}`)

    item.querySelector('.files-item__progress').value = progress
  }

  directUploadFailed (event) {
    event.preventDefault()
    const { id, error } = event.detail
    const item = this.listTarget.querySelector(`#files-item-direct-upload-${id}`)
    item.classList.remove('files-item--added', 'files-item--upload-pending', 'files-item--uploading')
    item.classList.add('files-item--upload-error')
    item.querySelector('.files-item__status').innerText = error
  }

  directUploadCompleted (event) {
    const { id } = event.detail
    const item = this.listTarget.querySelector(`#files-item-direct-upload-${id}`)
    item.classList.remove('files-item--added', 'files-item--upload-pending', 'files-item--uploading')
    item.classList.add('files-item--upload-complete')
    item.querySelector('.files-item__status').innerText = this.itemCompleteTextValue
  }
}
