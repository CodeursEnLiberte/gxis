import { Controller } from '@hotwired/stimulus'

export default class extends Controller {
  static targets = ['layerColorInput', 'rowColorInput', 'layerRadio', 'rowRadio']
  connect () {
    this.updateColorInputs()
  }

  updateColorInputs () {
    this.layerColorInputTarget.disabled = !this.layerRadioTarget.checked
    this.rowColorInputTarget.disabled = !this.rowRadioTarget.checked
  }
}
