import consumer from 'channels/consumer'

export default function onMapUpdate (mapId, callback, deletedFeature, deleteLayer) {
  consumer.subscriptions.create({ channel: 'MapUpdateChannel', map: mapId }, {
    received (data) {
      if (data.layer_id) {
        callback(data.layer_id)
      }
      if (data.deleted_feature) {
        deletedFeature(data.deleted_feature)
      }
      if (data.deleted_layer_id) {
        deleteLayer({ layerId: data.deleted_layer_id })
      }
    }
  })
}
