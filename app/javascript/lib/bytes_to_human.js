export default function (size) {
  // Use the unit option of Number.toLocaleString()
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/NumberFormat/NumberFormat#unit_2
  // https://tc39.es/ecma402/#table-sanctioned-single-unit-identifiers
  const units = ['byte', 'kilobyte', 'megabyte', 'gigabyte', 'terabyte', 'petabyte']
  // According to the SI, computers units use a base 1000 (a kB is 1000 bytes, an MB is 1000 kB, etc.)
  // However, some systems are still using the old base 1024 – like the Windows file explorer,
  // or Rails' NumberToHumanSizeConverter
  // To be consistent with Rails, we use a 1024-base here as well.
  const base = 1024

  let i = Math.floor(Math.log(size) / Math.log(base))
  i = Math.min(Math.max(i, 0), units.length - 1)
  const value = size / Math.pow(base, i)
  return value.toLocaleString(navigator.languages,
    { maximumFractionDigits: 1, style: 'unit', unit: units[i] })
}
